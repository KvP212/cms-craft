import { verify } from '../jwt'

export function User(req){

  function jwtVerify(cb){
    return verify(cb, req.cookies)
  }

  return {
    jwtVerify
  }
}

export const isConnected = (isLogged = true, privilege = 'user', redirecTo = '/') => (req, res, next) => {

  User(req).jwtVerify(user => {

    if( !user && !isLogged ){
      return next()

    } else if( user && isLogged){

      if(privilege === 'god'){
        if( user.privilege === 'god'){
          return next()
        } else {
          res.redirect(redirecTo)
        }
      } else if ( privilege === 'user' && user.privilege === 'user'){
        return next()
      }

    } else {
      res.redirect(redirecTo)
    }
  })
}

export default (req, res, next) => {
  res.user = User(req)

  return next()
}
