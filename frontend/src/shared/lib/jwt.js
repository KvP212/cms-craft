import jwt from 'jsonwebtoken'
import config from '@config'

const isDefined = variable => typeof variable !== 'undefined' && variable !== null

const isString = variable => isDefined(variable) && typeof variable === 'string'

const isJson = str => {
  if (!str || str === null) {
    return false
  }

  try {
    JSON.parse(str)
  } catch (e) {
    return false
  }

  return true
}

function getBase64(value){
  let buffer = false

  if(isString(value)){
    buffer = Buffer.from(value, 'base64').toString('ascii')
  }

  if(isJson(buffer)){
    buffer = JSON.parse(Buffer.from(value, 'base64').toString('ascii'))
  }

  return buffer
}

export function verify( cb, cookies = {}){
  const { security: { secretKey }} = config
  // const security = { secretKey: process.env.JWT_SECRET_KEY }
  const accessToken = cookies.at

  jwt.verify(accessToken, secretKey, (error, accessTokenData = {}) => {
    const { data: user } = accessTokenData

    if(error || !user ) {
      return cb(false)
    }

    return cb(getBase64(user))
  })
}

export async function getUserData( cookies ) {
  const UserPromise = new Promise( resolve => verify( user => resolve(user), cookies))
  const user = await UserPromise
  return user
}
