import React, { PureComponent } from 'react'

import styles from './Header.scss'

class Header extends PureComponent {
  render(){
    const {appName} = this.props
    return (
      <header className={styles.header}>
        <div className={styles.wrapper}>
          <div className={styles.logo}>
            <h1>{appName}</h1>
          </div>

          <div className={styles.userProfile}>

            <div className={styles.avatar}>
              <img src="/images/avatar.jpg" alt="Kev Pineda"/>
            </div>

            <div className={styles.name}>Kev Pineda</div>

          </div>

        </div>
      </header>
    )
  }
}

export default Header
