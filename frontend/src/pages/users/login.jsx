import React from 'react'
import { ApolloProvider } from 'react-apollo-hooks'
import { string } from 'prop-types'
import useApolloClient from '@apollo-client'

//Contexts
import FormProvider from '@contexts/Form'
import UserProvider from '@contexts/User'

import LoginLayout from '@users/components/Login/Layout'

const isBrowser = () => typeof window !== 'undefined'

const LoginPage = ({
  currentUrl = isBrowser() ? window.location.search.replace('?redirectTo=', '') : ''
}) => (
  <ApolloProvider client={(useApolloClient())}>
    <UserProvider>
      <FormProvider initialValues={{ email: '', password: ''}}>
        <LoginLayout currentUrl={currentUrl} />
      </FormProvider>
    </UserProvider>
  </ApolloProvider>
)

LoginPage.propTypes = {
  currentUrl: string
}

export default LoginPage
