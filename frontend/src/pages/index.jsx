import React from 'react'
import styles from './index.scss'

const App = () => <div className={styles.home}> Welcome to Next.js! </div>

export default App
