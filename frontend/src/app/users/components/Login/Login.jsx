import React, {useState, useEffect, useContext}from 'react'
import {Alert, DarkButton, PrimaryButton, Input, RenderIf} from 'fogg-ui'

import {FormContext} from '@contexts/Form'

import Logo from '@layouts/main/Logo'
import styles from './Login.scss'
import propTypes from '@propTypes'

const isBrowser = () => typeof window !== 'undefined'

function redirectTo(url= '/'){
  if( isBrowser() ){
    window.location.href = url
  }

  return false
}

function cx(...classes) {
  return classes.join(' ').trim()
}

const Login = ({login, currentUrl}) => {
  const [ready, setReady] = useState(false)

  useEffect(() => {
    setReady(true)
  }, [])

  const [errorMessage, setErrorMessage] = useState('')
  const [invalidLogin, setInvalidLogin] = useState(false)
  const oContext = useContext(FormContext)

  const handleLogin = async user => {
    const response = await login(user)

    if( response.error ){
      setInvalidLogin(true)
      setErrorMessage(response.message)
    } else {
      redirectTo(currentUrl || '/')
    }
  }

  return <>
    <RenderIf isTrue={invalidLogin}>
      <Alert danger center flat>
        {errorMessage}
      </Alert>
    </RenderIf>

      <div className={styles.login}>
        <div className={cx(styles.wrapper, ready ? styles.ready : '')}>
          <div className={styles.form}>
            <Logo center />

            <Input
              autoComplete='off'
              type='email'
              className={styles.email}
              name='email'
              placeholder='email'
              onChange={oContext.handleInputChange}
              value={oContext.values.email}
            />

            <Input
              autoComplete='off'
              type='password'
              className={styles.password}
              name='password'
              placeholder='password'
              onChange={oContext.handleInputChange}
              value={oContext.values.password}
            />

            <div className={styles.actions}>
              <div className={styles.left}>

                <DarkButton
                name="login"
                onClick={ () => handleLogin( oContext.values )}
                >
                  Login
                </DarkButton>

                &nbsp;

                <PrimaryButton name='register'>
                  register
                </PrimaryButton>

              </div>
            </div>
          </div>
        </div>
      </div>
  </>
}

Login.propTypes = {
  login: propTypes.login,
  currentUrl: propTypes.currentUrl
}

export default Login
