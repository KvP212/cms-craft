import React, {useContext} from 'react'
import Login from './Login'
import { UserContext } from '@contexts/User'
import propTypes from '@propTypes'
// Components
import Title from '@ui/Title'

const Layout = ({currentUrl}) => {
  const { login } = useContext(UserContext)

  return(
    <>
      <Title content="Login" />

      <Login login={login} currentUrl={currentUrl}/>
    </>
  )
}

Layout.propTypes = {
  currentUrl: propTypes.currentUrl
}

export default Layout
