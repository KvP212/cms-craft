import React, { useContext } from 'react'
import { useRouter } from 'next/router'
import { UserContext } from '@contexts/User'

import Header from '@layouts/dashboard/Header'
import Sidebar from '@layouts/dashboard/Sidebar'
import Content from '@layouts/dashboard/Content'
import styles from './Layout.scss'
import Title from '@ui/Title'

import Home from './modules/Home'
import Blog from './modules/Blog'
import Comments from './modules/Comments'
import Users from './modules/Users'

import config from '@config'

const isBrowser = () => typeof window !== 'undefined'

function getModuleInfo({ asPath }) {
  const [,page, module='home',action] = asPath.split('/')

  return {
    page,
    module,
    action
  }
}

const Layout = () => {
  const router = useRouter()
  const { page = 1 } = router.query
  const {user} = useContext(UserContext)
  const { module, action, id } = getModuleInfo(router)
  const moduleProps = {
    action,
    user,
    id,
    page
  }

  if (isBrowser() && !user) {
    return null
  }

  return (
    <>
      <Title content="Dashboard" />

      <main className={styles.layout}>

        <Header appName={config.appName}/>

        <div className={styles.wrapper}>

          <Sidebar module={module}/>

          <Content>
            {module === 'home' ? <Home {...moduleProps} /> : undefined}
            {module === 'blog' ? <Blog {...moduleProps} /> : undefined}
            {module === 'comments' ? <Comments {...moduleProps} /> : undefined}
            {module === 'users' ? <Users {...moduleProps} /> : undefined}
          </Content>

        </div>

      </main>
    </>
  )
}

export default Layout
