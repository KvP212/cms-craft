// Dependencies
import React, { Component } from 'react'
import { Alert, PrimaryButton, RenderIf, Input, Select, Tags, TextArea } from 'fogg-ui'
import propTypes from '@propTypes'

// Contexts
import { FormContext } from '@contexts/Form'

// Styles
import styles from './Create.scss'

import slug from 'slug'

function getRandomCode(max) {
  let code = ''
  let randomPoz
  const charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'

  max = max || 12

  for (let i = 0; i < max; i += 1) {
    randomPoz = Math.floor(Math.random() * charset.length)
    code += charset.substring(randomPoz, randomPoz + 1)
  }

  return code
}

function slugFn(str = '') {
  return slug(str, { lower: true })
}

class Create extends Component {
  state = {
    errorMessage: '',
    successMessage: '',
    messages: {},
    randomKey: getRandomCode()
  }

  handleCreate = async values => {

    const { create } = this.props
    const { clearValues } = this.context
    const response = await create(values)

    if (response.error) {
      this.setState({
        errorMessage: response.alert,
        successMessage: '',
        messages: response.messages
      })
    } else {
      clearValues(['title', 'slug', 'content', 'tags'])

      this.setState({
        successMessage: 'Created successfuly',
        errorMessage: '',
        messages: {},
        randomKey: getRandomCode()
      })
    }

  }

  renderFields = schema => {

    const { handleInputChange, setValue, values } = this.context
    const { messages, randomKey } = this.state

    return Object.keys(schema).map(field => {
      const { label, type, options, slug, theme, defaultValue } = schema[field]
      let currentValue = values[field]

      if (type === 'input') {
        if (!values[field]) {
          currentValue = defaultValue || values[field]
        }

        return (
          <div key={`div-${field}`} className={styles.inputBlock}>
            <div>
              <label className={styles.label}>{label}</label>
              {messages[field] ? <span className={styles.error}>{messages[field].msg}</span> : ''}
            </div>

            <Input
              key={`input-${field}`}
              autoComplete="off"
              name={field}
              onChange={e => {
                handleInputChange(e)

                if (slug) {
                  setValue('slug', slugFn(e.target.value))
                }
              }}
              value={currentValue}
              placeholder={`Add ${field} here`}
              style={{
                width: '75%'
              }}
            />

            {slug && (
              <div key="slug" className={styles.slug}>
                URL: /blog/<span>{slugFn(values[field])}</span>
              </div>
            )}
          </div>
        )
      }

      if (type === 'textarea') {
        return (
          <div key={`textarea-div-${field}`} className={styles.textAreaBlock}>
            <div>
              <label className={styles.label}>{label}</label>
              {messages[field] ? <span className={styles.error}>{messages[field].msg}</span> : ''}
            </div>

            <TextArea
              key={`textarea-${field}`}
              name={field}
              onChange={handleInputChange}
              placeholder={`Add ${field} here`}
              style={{
                height: '300px',
                width: '75%'
              }}
            >
              {values[field]}
            </TextArea>
          </div>
        )
      }

      if (type === 'select') {
        if (!options) {
          return null
        }

        return (
          <div key={`div-select-${field}`} className={styles.selectBlock}>
            <div>
              <label className={styles.label}>{label}</label>
              {messages[field] ? <span className={styles.error}>{messages[field].msg}</span> : ''}
            </div>

            <Select
              label={`Select ${field}`}
              name={field}
              type={theme}
              onClick={({ value }) => {
                setValue(field, value)
              }}
              options={options}
            />
          </div>
        )
      }

      if (type === 'tags') {
        return (
          <div key="div-tags">
            <div>
              <label className={styles.label}>{label}</label>
              {messages[field] ? <span className={styles.error}>{messages[field].msg}</span> : ''}
            </div>

            <Tags key={`tags-${randomKey}`} getTags={tags => setValue(field, tags)} />
          </div>
        )
      }

      return null
    })

  }

  render() {
    const { values } = this.context
    const { caption, schema = false } = this.props
    const { errorMessage, successMessage } = this.state

    if (!schema) {
      return <p>You need to define a schema</p>
    }

    return (
      <div className={styles.create}>
        <h1>Create {caption}</h1>

        <RenderIf isTrue={errorMessage}>
          <Alert danger center flat style={{ display: 'inline-block' }}>
            {errorMessage}
          </Alert>
        </RenderIf>

        <RenderIf isTrue={successMessage}>
          <Alert success center flat style={{ display: 'inline-block' }}>
            {successMessage}
          </Alert>
        </RenderIf>

        <form>
          {this.renderFields(schema)}

          <PrimaryButton
            onClick={() => this.handleCreate(values)}
          >
            Save
          </PrimaryButton>
        </form>
      </div>
    )
  }
}

Create.contextType = FormContext

Create.propTypes = {
  create: propTypes.create,
  caption: propTypes.caption,
  schema: propTypes.schema
}

export default Create
