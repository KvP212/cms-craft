import express from 'express'
import next from 'next'
import path from 'path'
import bodyParser from 'body-parser'
import cookiesParser from 'cookie-parser'
import cors from 'cors'
import session from 'express-session'

//midd
import user, {isConnected} from '@middlewares/user'

//Settings Nextjs
const dev = process.env.NODE_ENV !== 'production'
const nextApp = next({ dev })
const handle = nextApp.getRequestHandler()

//Run
nextApp.prepare().then(() => {
  const app = express()

  //Public Static
  app.use(express.static(path.join(__dirname, '../public')))

  //midd
  app.use(session({
    resave: false,
    saveUninitialized: true,
    secret: process.env.JWT_SECRET_KEY
  }))
  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({extended: false}))
  app.use(cookiesParser(process.env.JWT_SECRET_KEY))
  app.use(cors({credentials: true, origin: true}))
  app.use(user)

  //Routes
  app.get('/login', isConnected(false), (req, res) => {
    return nextApp.render(req, res, '/users/login', req.query)
  })

  app.get('/logout', (req, res) => {
    res.clearCookie('at')
    res.redirect('/')
  })

  app.get(
    '/dashboard/:module?/:action?/:id?',
    isConnected(
      true,
      'god',
      '/login?redirectTo=/dashboard'),
    (req, res) => {
      const {module='home'} = req.params

      return nextApp.render(req, res, `/dashboard/${module}`, req.query)
    }
  )

  app.all( '*', (req, res) => handle(req, res) )

  app.listen( process.env.PORT || 3000 )
})

