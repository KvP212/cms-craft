import React, { createContext, useState } from 'react'
import { useApolloClient } from 'react-apollo-hooks'
import { useCookies } from 'react-cookie'
import propTypes from '@propTypes'
import { getUserData } from '@lib/jwt'

import LOGIN_MUTATION from '@graphql/user/login.mutation'

const getGraphQlError = error => {
  return {
    error: true,
    message: error.toString().replace('Error: GraphQL error: ', '')
  }
}

export const UserContext = createContext({
  getUser: () => undefined,
  login: async () => undefined,
  user: {}
})

const UserProvider = ({children}) => {
  const { mutate } = useApolloClient()
  const [, setCookie] = useCookies()
  const [user, setUser] = useState()

  getUser()

  async function getUser() {
    const [cookies] = useCookies()
    const userData = await getUserData(cookies)

    if(!user){
      setUser(userData)
    }

    return userData
  }

  async function login({email, password}) {
    try {
      const {data} = await mutate({
        mutation: LOGIN_MUTATION,
        variables: {
          email,
          password
        }
      })

      if (data) {
        setCookie('at', data.login.token, {path: '/'})
        setUser(data.login.token)

        return data.login.token
      }

    } catch (err) {
      return getGraphQlError(err)
    }
  }

  const context = {
    getUser,
    login,
    user
  }

  return (
    <UserContext.Provider value={context}>
      {children}
    </UserContext.Provider>
  )
}

UserProvider.propTypes = {
  children: propTypes.children
}

export default UserProvider
