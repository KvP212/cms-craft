// Dependencies
import React, { createContext, useState } from 'react'
import propTypes from '@propTypes'
import { useApolloClient } from 'react-apollo-hooks'


// Mutations
import CREATE_POST from '@graphql/blog/createPost.mutation'

// Queries
import GET_POSTS from '@graphql/blog/getPosts.query'
import GET_POSTS_COUNT from '@graphql/blog/getPostsCount.query'

import validations from '@validations/blog'

const validateFields = (validations, values, alert = 'Error trying to create the item') => {
  const messages = {}

  Object.keys(validations).forEach(field => {
    const { len, isEmpty } = validations[field]

    if (len && len.arg) {
      if (values[field].length !== len.arg) {
        messages[field] = {
          msg: len.msg
        }
      }
    }

    if (isEmpty) {
      if (values[field] === '' || !values[field]) {
        messages[field] = {
          msg: isEmpty.msg
        }
      }
    }
  })

  return Object.keys(messages).length > 0
    ? {
      error: true,
      alert,
      messages
    }
    : {
      error: false
    }
}
export const BlogContext = createContext({
  create: async () => undefined,
  read: async () => undefined,
  posts: []
})

const BlogProvider = ({ children }) => {
  const { mutate, query } = useApolloClient()
  const [posts, setPosts] = useState([])


  async function create(values) {
    let messages = validateFields(validations, values, 'Error trying to create the post')

    if (messages.error) {
      return messages
    }

    const { errors, data } = await mutate({
      mutation: CREATE_POST,
      variables: values,
      errorPolicy: 'all'
    })

    if (errors && errors.length > 0) {
      const [{ extensions: { exception } }] = errors

      if (exception.name === 'SequelizeUniqueConstraintError') {
        messages = {
          error: true,
          alert: `The post with title "${values.title}" already exists`,
          messages: {
            title: {
              msg: 'Change the title'
            }
          }
        }
      }
    }

    if (messages.error) {
      return messages
    }

    if (data) {
      return data.createPost
    }
  }

  async function read(page) {
    const { data: count } = await query({
      query: GET_POSTS_COUNT
    })

    const { data } = await query({
      query: GET_POSTS,
      variables: {
        orderBy: 'createdAt',
        direction: 'DESC',
        limit: 10,
        offset: page === 1 ? 0 : (page - 1) * 10
      }
    })

    if (count && data) {
      setPosts(data.getPosts)

      return {
        count: count.getPostsCount.count,
        data: data.getPosts
      }
    }
  }

  const context = {
    create,
    read,
    posts
  }

  return (
    <BlogContext.Provider value={context}>
      {children}
    </BlogContext.Provider>
  )
}

BlogProvider.propTypes = {
  children: propTypes.children.isRequired
}

export default BlogProvider
