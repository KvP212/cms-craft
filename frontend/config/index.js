//Config
import common from './common.json'
import local from './local.json'
import production from './production.json'

//Config by env
const config = {
  local: {
    ...common,
    ...local
  },
  production: {
    ...common,
    ...production
  }
}

//development => local
let env = 'local'

if( process.env.NODE_ENV && process.env.NODE_ENV !== 'development'){
  env = process.env.NODE_ENV
}

//validation env
export const isLocal = () => env === 'local'
export const isProduction = () => env === 'production'

//config
export default config[env]
