# CMS :: POSTGRES - REACTJS - NEXTJS - GRAPHQL

_Manejando base de datos con postgres, consultando por medio de GraphQL y realizando Server-Side Rendering con Nextjs, para crear el CMS con componentes reutilizables_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
Node.js
File .ENV
Folder BackEnd
Folder FrontEnd
```
_Install_
```
BackEnd/ npm install
FrontEnd/ npm install
```

## Ejecutando las pruebas ⚙️

_Ejecutar las pruebas automatizadas para este sistema_

_Dev_
```
BackEnd/ npm test

BackEnd/ npm run dev
FrontEnd/ npm run dev
```
_Production_
```
1) BackEnd/ npm run bb
2) BackEnd/ npm start

1) FrontEnd/ npm run build
```

## Deployment 📦

```
Nodemon - npm install nodemon -D
Babel - npm install --save-dev @babel/cli @babel/core @babel/preset-env @babel/node @babel/plugin-transform-runtime
```

## Construido con 🛠️

_Frameworks_

* [NodeJS](https://nodejs.org/es/) - Server BackEnd
* [GraphQL](https://graphql.org/) - SQL BackEnd
* [ReactJS](https://es.reactjs.org/) - Lib FrontEnd
* [NextJS](https://nextjs.org//) - Framework Server-Side

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **Kev Pineda** - *Trabajo Inicial y Desarrollo* - [KvP212](https://gitlab.com/KvP212)

También puedes mirar la lista de todos los [contribuyentes](https://gitlab.com/KvP212/cms-craft/-/project_members) quíenes han participado en este proyecto.
