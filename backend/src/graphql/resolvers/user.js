import { doLogin } from '../../utils/auth'

export default {
  Query: {
    getUsers: (_, {}, {models}) => {
      return models.User.findAll({
        include: [{
          model: models.Post,
          as: 'posts',
          include:[{
            model: models.Tag,
            as: 'tags'
          }]
        }]
      })
    }
  },
  Mutation: {
    createUser: (_, {input}, {models}) => models.User.create({...input}),
    login: (_, {input: {email, password}}, {models}) => doLogin(email, password, models)
  }
}
