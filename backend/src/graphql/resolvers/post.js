export default {
  Query: {
    getPosts: (_, {options = {} }, {models}) => {
      const {
        orderBy = 'createdAt',
        direction = 'DESC',
        limit = false,
        offset = false
      } = options

      const args = {
        order: [[orderBy, direction]],
        include:[{
          model: models.Tag,
          as: 'tags'
        }]
      }

      if( limit > 0){
        args.limit = limit
      }

      if (offset > 0){
        args.offset = offset
      }

      return models.Post.findAll(args)
    },

    getPostsCount: (_, {}, {models}) => {
      return models.Post.count().then( count => ({count}))
    }
  },
  Mutation: {
    createPost: (_, {input}, {models}) => models.Post.create({...input}, {
      include: [{
        model: models.Tag,
        as: 'tags'
      }]
    })
  }
}
