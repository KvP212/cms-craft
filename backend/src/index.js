import { ApolloServer, makeExecutableSchema, gql } from 'apollo-server'
import models from './models'

//type Definitions
import resolvers from './graphql/resolvers'

//Resolvers
import typeDefs from './graphql/types'

//Schema
const schema = makeExecutableSchema({typeDefs, resolvers})


//Apollo Server
const apolloServer = new ApolloServer({
  schema,
  context:{
    models
  }
})

//Run Apollo Server
const alter = true
const force = false

models.sequelize
  .sync({alter, force})
  .then(()=> {
    apolloServer.listen( process.env.PORT || 5000)
      .then(({url}) => console.log(`Running on ${url}`))
  })
