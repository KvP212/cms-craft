const config = {
  "serverPort": process.env.PORT,
  "db":{
    "host": process.env.DB_PG_HOST,
    "dialect": "postgres",
    "database": "cms",
    "username": process.env.DB_PG_USER,
    "password": process.env.DB_PG_PASS
  },
  "security": {
    "secretKey": "some",
    "expiresIn": "7d"
  }
}

//configurations
export const $db = () => config.db
export const $security = () => config.security
export const $serverPort = () => config.serverPort

